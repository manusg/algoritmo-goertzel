/*****************************************************************************/
/*                                                                           */
/* FILENAME                                                                  */ 
/*   goertzel.h                                                              */                           
/*                                                                           */
/* DESCRIPTION                                                               */
/*   Header file for Goertzel algorithm.                                     */
/*                                                                           */
/* REVISION                                                                  */
/*   Revision: 1.00                                                          */ 
/*   Author  : Richard Sikora                                                */ 
/*---------------------------------------------------------------------------*/
/*                                                                           */
/* HISTORY                                                                   */
/*   Revision 1.00                                                           */
/*   12th April 2010. Created by Richard Sikora from TMS320C5510 DSK code.   */
/*                                                                           */
/*****************************************************************************/
/*
 * Copyright (C) 2010 Texas Instruments Incorporated - http://www.ti.com/ 
 * 
 * 
 *  Redistribution and use in source and binary forms, with or without 
 *  modification, are permitted provided that the following conditions 
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright 
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the   
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
*/

#ifndef GOERTZEL_H
#define GOERTZEL_H

 #define COEFFICIENT_697_Hz  27980
 #define COEFFICIENT_770_Hz  26956
 #define COEFFICIENT_852_Hz  25701
 #define COEFFICIENT_941_Hz  24219

 #define COEFFICIENT_1209_Hz 19073
 #define COEFFICIENT_1336_Hz 16325
 #define COEFFICIENT_1477_Hz 13085
 #define COEFFICIENT_1633_Hz  9315

 /* The following are the bit masks corresponding to each button push */

 #define BUTTON_1    0x0011
 #define BUTTON_2    0x0021
 #define BUTTON_3    0x0041
 #define BUTTON_4    0x0012
 #define BUTTON_5    0x0022
 #define BUTTON_6    0x0042
 #define BUTTON_7    0x0014
 #define BUTTON_8    0x0024
 #define BUTTON_9    0x0044
 #define BUTTON_STAR 0x0018
 #define BUTTON_0    0x0028
 #define BUTTON_HASH 0x0048

 void goertzel_filter( short int * delay, short int input, short int coefficient );
  
 short int calculate_goertzel_output_power( short int * delay, short int coefficient );
 
 short int goertzel_single_frequency ( short int input, short int coefficient);

#endif

/*****************************************************************************/
/* End of goertzel.h                                                         */
/*****************************************************************************/
